$("button").on("click", function () {
    $("button").addClass("_hide");

    $("body").append("<input/ class='size-input' placeholder='Введите размер круга'>");
    $("body").append("<input/ class='color-input' placeholder='Введите цвет круга'>");
    $("<div></div>", { class: "accept", text: "Подтвердить", css: { backgroundColor: "red", width: "100px", height: "30px" }, appendTo: "body" });

    $(".accept").on("click", function () {
        let size = $(".size-input").val();
        let color = true;
        if (($(".color-input").val().indexOf(",") > 1) && ($(".color-input").val().indexOf("%") > 1)) {
            color = "hsl(" + $(".color-input").val() + ")";
        } else if ($(".color-input").val().indexOf(",") > 1) {
            color = "rgb(" + $(".color-input").val() + ")";

        } else {
            color = $(".color-input").val();
        }
        $(".size-input,.color-input,.accept").addClass("_hide");
        $("<div></div>", { css: { backgroundColor: color, width: size, height: size, borderRadius: size / 2 }, appendTo: "body" });
        console.log(size, color);
    });
});
